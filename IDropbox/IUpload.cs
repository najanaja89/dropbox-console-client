﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dropbox.Api;

namespace IDropbox
{
    public interface IUpload
    {
        void Upload(DropboxClient dropboxClient, string folder, string file, string content);
    }
}
